# README #

This README would normally document whatever steps are necessary to get the proxy with forwarding to two different server.

### How testing my assignment? ###

* python proxy.py
* python restAPIwithmySQL0.py
* python restAPIwithmySQL1.py
* POST to localhost:9090 / or GET existing ID to localhost:9090
* u can notice proxy forward to last server in the node_list
* close restAPIwithmySQL1.py session
* u can notice proxy forward to restAPIwithmySQL0.py server
* close restAPIwithmySQL0.py session
* u can notice proxy server is opening circuit breaker, waiting for restAPIwithmySQL server back
* python restAPIwithmySQL0.py
* u can notice the proxy server restart to forward to python restAPIwithmySQL server again.

### I also attach the snapshot in the file ###