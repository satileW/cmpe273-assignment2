from flask import Flask, request, flash, Markup, jsonify, make_response, abort, json, Response
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:123456@localhost/mydatabase'
#app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:123456@database/selfdatabase'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
#app.config["JSON_SORT_KEYS"] = False
db = SQLAlchemy(app)

class Orders(db.Model):
	__tablename__ = 'order'

	id = db.Column('id', db.Integer, primary_key=True)
	name = db.Column('name', db.String(30))
	email = db.Column('email', db.String(320))
	category = db.Column('category', db.String(100))
	description = db.Column('description', db.String(100))
	link = db.Column('link', db.String(300))
	estimated_costs = db.Column('estimated_costs', db.String(20))
	submit_date = db.Column('submit_date', db.String(20))
	status = db.Column('status', db.String(20))
	decision_date = db.Column('decision_date', db.String(20))

	def __init__(self, id, name, email, category, description, link, estimated_costs, submit_date):
		self.id = id
		self.name = name
		self.email = email
		self.category = category
		self.description = description
		self.link = link
		self.estimated_costs = estimated_costs
		self.submit_date = submit_date

@app.route("/")
def hello():
	return "Hello assignment2-0"

@app.errorhandler(404)  
def page_not_found(error):  
    return 'This page does not exist', 404 

@app.route('/v1/expenses/<int:postID>', methods = ['GET', 'PUT', 'DELETE'])
def op_get_put_del(postID):
	if request.method == 'GET':
		sql_get = Orders.query.filter_by(id=postID).first()

		if sql_get is None: 
			abort(404)
		
		return jsonify(id=sql_get.id,
					 name=sql_get.name,
					 email=sql_get.email,
					 category=sql_get.category,
					 description=sql_get.description,
					 link=sql_get.link,
					 estimated_costs=sql_get.estimated_costs,
					 submit_date=sql_get.submit_date,
					 status=sql_get.status,
					 decision_date=sql_get.decision_date), 200

	elif request.method == 'PUT':
		sql_put = Orders.query.filter_by(id=postID).first()
		if sql_put is None: 
			abort(404)

		req_put = json.loads(request.data)
		keys = req_put.keys()
		for item in keys:
			if item == 'email':
				sql_put.email = req_put['email']
			elif item == 'estimated_costs':
				sql_put.estimated_costs = req_put['estimated_costs']
			elif item == 'status':
				sql_put.status = req_put['status']
		db.session.commit()
		res = Response(status=202)
		return res
	
	elif request.method == 'DELETE':
		sql_del = Orders.query.filter_by(id=postID).first()
		if sql_del is None: 
			abort(404)

		db.session.delete(sql_del)
		db.session.commit()
		res = Response(status=204)
		return res
		
@app.route('/v1/expenses', methods = ['POST'])
def op_post():
	if request.method == 'POST':
		req_post = json.loads(request.data)
		order = Orders(id = req_post['id'] , name=req_post['name'], email=req_post['email'], category=req_post['category'], description=req_post['description'], link=req_post['link'], estimated_costs=req_post['estimated_costs'], submit_date=req_post['submit_date'])
		order.status = 'pending'
		db.session.add(order)
		db.session.commit()
		
		return jsonify(id=order.id,
					   name=order.name,
					   email=order.email,
					   category=order.category,
					   description=order.description,
					   link=order.link,
					   estimated_costs=order.estimated_costs,
					   submit_date=order.submit_date,
					   status=order.status,
					   decision_date=order.decision_date), 201	


if __name__ == '__main__':
	db.create_all()
	app.run(host="0.0.0.0", port=5000, debug=True)