#!/usr/bin/python
# This is a simple port-forward / proxy, written using only the default python
# library. If you want to make a suggestion or fix something you can contact-me
# at voorloop_at_gmail.com
# Distributed over IDC(I Don't Care) license
import socket
import select
import time
import sys

# Changing the buffer_size and delay, you can improve the speed and bandwidth.
# But when buffer get to high or delay go too down, you can broke things
buffer_size = 4096
#delay = 0.0001
delay = 1
forward_to = ('127.0.0.1',5000)
forward_too = ('127.0.0.1',5001)

node_list = [forward_to, forward_too]

from time import sleep
import cb_ans

@cb_ans.CircuitBreaker(name='My_PROXY_CB', max_failure_to_open=3, reset_timeout=3)
def forward_to_restAPI():
    ip = node_list[len(node_list)-1][0]
    port = node_list[len(node_list)-1][1]
    forward = Forward().start(ip, port)
    if forward:
        return forward
    else:
        raise Exception("Connection error, no restfulAPI service")
   
class Forward:
    def __init__(self):
        self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print 'initail'

    def start(self, host, port):
        try:
            self.forward.connect((host, port))
            return self.forward
        except Exception, e:
            print e
            return False

class TheServer:
    input_list = []
    channel = {}

    def __init__(self, host, port):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((host, port))
        self.server.listen(200)

        #use anthor port to reput the forward_to server
        self.preserver_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.preserver_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.preserver_server.bind((host, 9091))
        self.preserver_server.listen(200)

    def main_loop(self):
        self.input_list.append(self.server)
        self.input_list.append(self.preserver_server)
        count = 0
        while 1:
            #time.sleep(delay)
            ss = select.select #non blocking
            inputready,outputready,exceptready = ss(self.input_list, [], [], delay)
            for self.s in inputready:
                if self.s == self.server:
                    try:
                        self.on_accept(count)
                    except Exception, e:
                        node_list.append(forward_to)
                        #print e.message
                    break
                elif self.s == self.preserver_server:
                    self.accept_new_node()
                    break
                
                self.data = self.s.recv(buffer_size)
                print self.data
                if len(self.data) == 0:
                    self.on_close()
                    break
                else:
                    self.on_recv()
            
            count += 1
            if(count > 4294967296):
                count = 0

    def accept_new_node():
        node_list.append(forward_to)

    def on_accept(self, count):
        if(len(node_list) == 0):
            self.server.accept()
            #import os
            #os.system("python restAPIwithmySQL0.py")
            raise Exception('Node list is Empty')

        try:
            forward = forward_to_restAPI()
        except Exception, e:
            #print e
            if(str(e.message).find('CircuitBreaker') == 0):
                #del node_list[len(node_list)-1]
                if len(node_list) > 0:
                    node_list.remove(len(node_list)-1)
            return
        
        clientsock, clientaddr = self.server.accept()
        print clientaddr, "has connected"
        self.input_list.append(clientsock)
        self.input_list.append(forward)
        self.channel[clientsock] = forward
        self.channel[forward] = clientsock



    def on_close(self):
        print self.s.getpeername(), "has disconnected"
        #remove objects from input_list
        self.input_list.remove(self.s)
        self.input_list.remove(self.channel[self.s])
        out = self.channel[self.s]
        # close the connection with client
        self.channel[out].close()  # equivalent to do self.s.close()
        # close the connection with remote server
        self.channel[self.s].close()
        # delete both objects from channel dict
        del self.channel[out]
        del self.channel[self.s]

    def on_recv(self):
        data = self.data
        # here we can parse and/or modify the data before send forward
        #print data
        self.channel[self.s].send(data)

if __name__ == '__main__':
        server = TheServer('127.0.0.1', 9090)# this is my proxy
        try:
            server.main_loop()            
            
        except KeyboardInterrupt:
            print "Ctrl C - Stopping server"
            sys.exit(1)